<?php

namespace App\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\UserBundle\Entity\User;

class DefaultController extends FOSRestController
{
    /**
     * @Route("/login/register", name="register")
     * @Method({"POST"})
     *
     * @TODO: Wyłączyć dodawanie użytkowników
     */
    public function registerAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $encoder = $this->container->get('security.password_encoder');

        $username = $request->request->get('_username');
        $password = $request->request->get('_password');

        $user = new User($username);
        $user->setPassword($encoder->encodePassword($user, $password));

        $em->persist($user);
        $em->flush($user);

        return new Response(sprintf('User %s successfully created', $user->getUsername()));
    }

    /**
     * @Route("/login/logout", name="logout")
     */
    public function logoutAction()
    {
        $user = $this->getUser();

        //
        var_dump( $user );

        //
        if( $user )
        {
            return new Response(sprintf('Logged in as %s', $user->getUsername()));
        }

        return new Response('You was not logined.');
    }
}
