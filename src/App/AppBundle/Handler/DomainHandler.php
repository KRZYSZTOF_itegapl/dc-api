<?php
/**
 * Created by PhpStorm.
 * User: KRZYSZTOF
 * Date: 2018-03-16
 * Time: 15:15
 */

namespace App\AppBundle\Handler;


use Doctrine\Common\Persistence\ObjectManager;

class DomainHandler
{
    protected $objectManager;
    protected $domainRepository;

    public function __construct( ObjectManager $objectManager )
    {
        $this->objectManager = $objectManager;
        $this->domainRepository = $objectManager->getRepository('AppAppBundle:Domain');
    }

    public function getDomainsCount( $parameters )
    {
        return $this->domainRepository->findDomainsCount( $parameters );
    }

    public function getDomains( $parameters )
    {
        return $this->domainRepository->findDomains( $parameters );
    }

    public function get( $id )
    {
        return $this->domainRepository->find( $id );
    }


}