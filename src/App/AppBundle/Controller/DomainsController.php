<?php

namespace App\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\SerializationContext;


/**
 * @REST\RouteResource("Domains")
 */
class DomainsController extends FOSRestController
{
    /**
     * @REST\QueryParam(name="limit", requirements="\d+", strict=true)
     * @REST\QueryParam(name="offset", requirements="\d+", strict=true)
     * @REST\QueryParam(name="order")
     * @REST\QueryParam(name="filter")
     *
     * @REST\View(
     *      templateVar="product",
     *      serializerGroups={"domains"}
     * )
     */
    public function getAction( ParamFetcherInterface $paramFetcher )
    {
        $domainHandler = $this->get('domain.handler');

        //
        $parameters = [
                'limit' => $paramFetcher->get('limit'),
                'offset' => $paramFetcher->get('offset'),
                'order' => $paramFetcher->get('order'),
                'filter' => $paramFetcher->get('filter'),
            ];

        // :: RETURN ::
        return [
            'count' => $domainHandler->getDomainsCount([
                'filter' => $parameters['filter'],
                'order' => $parameters['order'],
            ]),
            'data' => $domainHandler->getDomains( $parameters ),
        ];
    }
}