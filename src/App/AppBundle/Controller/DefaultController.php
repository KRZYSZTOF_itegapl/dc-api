<?php

namespace App\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\Serializer\SerializationContext;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $serializer = $this->get('jms_serializer');

        //
        $query = $entityManager->createQuery('SELECT domain FROM App\AppBundle\Entity\Domain domain WHERE 1=1');
        $result = $query->getResult();

        //
        $jsonData = $serializer->toArray( $result, SerializationContext::create()->setGroups(array('domains')) );

        echo '<pre>';
        var_dump( $jsonData );
        echo '</pre>';



        return $this->render('@AppApp/Default/index.html.twig');
    }
}
