<?php

namespace App\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\SerializationContext;

/**
 * @REST\RouteResource("Domain")
 */
class DomainController extends FOSRestController
{
    /**
     * @REST\Get("/domain/{id}")
     * @REST\View(
     *      templateVar="product",
     *      serializerGroups={"domainDetails"}
     * )
     */
    public function getAction( $id )
    {
        return $this->getOr404( $id );
    }

    protected function getOr404( $id )
    {
        if(!( $domain = $this->get('domain.handler')->get($id) ))
        {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $domain;
    }

}
