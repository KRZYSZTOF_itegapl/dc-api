<?php

namespace App\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use App\AppBundle\Entity\Company;

/**
 * Domian
 *
 * @ORM\Table(name="domian")
 * @ORM\Entity(repositoryClass="App\AppBundle\Repository\DomianRepository")
 */
class Domain
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"domains","domainDetails"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Serializer\Groups({"domains","domainDetails"})
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Company", mappedBy="domain")
     * @Serializer\Groups({"domainDetails"})
     */
    private $companies;

    public function __construct()
    {
        $this->companies = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Domain
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add company
     *
     * @param \App\AppBundle\Entity\Company $company
     *
     * @return Domain
     */
    public function addCompany( Company $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \App\AppBundle\Entity\Company $company
     */
    public function removeCompany( Company $company )
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("company")
     * @Serializer\Groups({"domains"})
     */
    public function getNewestCompany()
    {
        return $this->companies->last();
    }
}
